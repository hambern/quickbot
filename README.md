# QuickBot: A Simple PHP Chatbot

## Table of Contents

- [Overview](#overview)
- [Features](#features)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Directory Structure](#directory-structure)
- [Code Explanation](#code-explanation)
- [Contributing](#contributing)
- [License](#license)
  
## Overview

QuickBot is a straightforward PHP-based chatbot designed for educational purposes. It uses simple Natural Language Processing (NLP) techniques to understand user input and generate appropriate responses. QuickBot serves as an excellent introduction to NLP and chatbot development.

## Features

- Simple NLP for intent recognition
- Multi-language support through JSON-based corpora
- Easy customization

## Prerequisites

- PHP 7.4 or higher
- Web server (e.g., Apache or Nginx)
- Basic understanding of PHP and HTML

## Installation

1. **Clone the Repository**:
```bash
git clone https://gitlab.com/hambern/quickbot.git
```

2. **Navigate to Project Folder**:
```bash
cd quickbot
```

3. **Set Up Web Server**: Make sure your web server is configured to serve the QuickBot directory.

4. **Permissions**: Set the appropriate permissions if required.

5. **Access QuickBot**: Open your web browser and access `index.php`.

## Usage

1. **Open the Chat Interface**: Access QuickBot through your web server.

2. **Start Chatting**: Type your query into the input box and press Enter.

3. **Train Your Model**: QuickBot operates using a dataset referred to as a "corpus," which is organized into "intents." Each intent comprises example queries, known as "utterances," and corresponding responses, known as "answers". Based on your utterance Quickbot tries to determine what your intent is based on the sample utterances in `corpus.json`. When your intent is determined it picks a random answer to that intent. So, to train the bot you have to update `corpus.json` and add new intents, utterances and answers. With each addition the bot gets smarter and smarter.

## Directory Structure

```
├── assets
│   ├── logo.png
│   ├── script.js
│   └── style.css
├── bot.php
├── data
│   └── intents.json
├── index.html
├── README.md
└── src
    ├── nlp
    │   ├── IntentResolver.php
    │   └── Tokenizer.php
    └── QuickBot.php
```

## File Explanation

- `intents.json`: Contains the intents, utterances, and responses.
- `IntentResolver.php`: Responsible for intent recognition.
- `Tokenizer.php`: Tokenizes the user input.
- `QuickBot.php`: Main class that brings everything together.
- `bot.php`: Handles the request to the bot.
- `index.html`: The entry point of the application.

## Contributing

Contributions are welcome!

## License

This project is licensed under the MIT License.