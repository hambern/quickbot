<?php

require_once 'src/QuickBot.php';

$quickBot = new QuickBot("data/intents.json");

$input = json_decode(file_get_contents("php://input"), true);

$utterance = $input['utterance'] ?? '';

$response = $quickBot->getResponse($utterance);

header('Content-type: application/json');

echo json_encode($response);
