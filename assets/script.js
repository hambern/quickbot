const { createApp, ref, nextTick } = Vue

createApp({
    setup() {
        const messages = ref([])
        const utterance = ref('')

        function addMessage(message) {
            messages.value.push({ agent: 'user', utterance: message })

            fetch('bot.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ utterance: message }),
            })
            .then(response => response.json())
            .then(data => {
                messages.value.push({ agent: 'bot', utterance: data })
                scrollToBottom();
            })
            .catch(error => {
                console.error(error)
            });

            utterance.value = ''
        }

        function scrollToBottom() {
            nextTick(() => {
                window.scrollTo(0, document.body.scrollHeight)
            });
        }

        addMessage('Hi')

        return { messages, addMessage, utterance }
    }
}).mount('#app')
