<?php

class Tokenizer
{
    public function tokenize(string $text, bool $toLowerCase = true): array
    {
        $text = $toLowerCase ? strtolower(trim($text)) : trim($text);
        return preg_split('/\s+/', $text, -1, PREG_SPLIT_NO_EMPTY);
    }
}
