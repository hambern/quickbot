<?php

class IntentResolver
{
    private $intents;

    public function __construct($intents)
    {
        $this->intents = $intents;
    }

    private function jaccardSimilarity($tokensA, $tokensB): float
    {
        $intersection = count(array_intersect($tokensA, $tokensB));
        $union = count(array_unique(array_merge($tokensA, $tokensB)));
        return $union > 0 ? $intersection / $union : 0;
    }

    public function predictIntent($tokens): array
    {
        $bestMatch = null;
        $highestScore = 0;

        foreach ($this->intents as $intent) {
            foreach ($intent['patterns'] as $pattern) {
                $patternTokens = explode(' ', $pattern);
                $score = $this->jaccardSimilarity($tokens, $patternTokens);

                if ($score > $highestScore) {
                    $highestScore = $score;
                    $bestMatch = $intent;
                }
            }
        }

        return $bestMatch ? [
            'intent' => $bestMatch['tag'],
            'response' => $bestMatch['responses'][array_rand($bestMatch['responses'])],
            'score' => $highestScore
        ] : [];
    }
}
