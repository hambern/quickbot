<?php
// Include other classes
require_once 'nlp/IntentResolver.php';
require_once 'nlp/Tokenizer.php';

class QuickBot {
    private $intentResolver;
    private $tokenizer;

    public function __construct($corpusPath)
    {
        // Load corpus data from JSON file
        $json = file_get_contents($corpusPath);
        $intents = json_decode($json, true);

        // Initialize NLP components
        $this->intentResolver = new IntentResolver($intents);
        $this->tokenizer = new Tokenizer();
    }


    public function getResponse($userInput): string
    {
        // Step 1: Tokenize the user input
        $tokens = $this->tokenizer->tokenize($userInput);

        // Step 2: Predict the intent based on tokens
        $predictedIntent = $this->intentResolver->predictIntent($tokens);

        // Step 3: Generate a response based on the predicted intent
        $response = $predictedIntent ? $predictedIntent['response'] : "I'm sorry, I don't understand.";

        return $response;
    }
}
